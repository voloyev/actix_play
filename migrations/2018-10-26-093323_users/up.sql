-- Your SQL goes here
create table users (
       email varchar(100) not null primary key,
       password varchar(64) not null,
       created_at timestamp not null
);
