-- Your SQL goes here
create table invitations (
       id uuid not null primary key,
       email varchar(100) not null,
       expires_at timestamp not null       
);
